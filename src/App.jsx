import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';
import { Col, Row } from 'react-bootstrap';
import Header from './components/headers';
import Menu from './components/Menu';
import TableContainer from './components/Table';
import main from './DataJson/main';

class MenuContainer extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      showCollapse: false,
      trigger: {},
    };
  }

  openCollapse = (showCollapses, data) => () => {
    const { showCollapse } = this.state;

    this.setState({ showCollapse: !showCollapse, trigger: { showCollapse, data } });

  }

  render() {
    const { showCollapse, trigger } = this.state;
    return (
      <div>
        {
       main.contents.map((res) => <Menu
         key={Math.random()}
         data={res}
         openCollapse={this.openCollapse(showCollapse, res.name)}
         trigger={trigger}
         showCollapse={showCollapse}
       />)
     }
      </div>
    );
  }
}


const App = () => {

  const styles = {
    textPosition: {
      textAlign: 'left',
      marginTop: 15,
      marginBottim: 15,
    },
    container: {
      width: 900,
      margin: '0 auto',
      marginTop: 10,
      marginBottom: 10,
    },
  };
  return (
    <div className="App">
      <Header />
      <Row>
        <Col>
          <div style={styles.container}>
            <h2 style={styles.textPosition}>Products</h2>
          </div>
        </Col>
      </Row>

      <Router>
        <Switch>
          <Route exact path="/" component={MenuContainer} />
          <Route exact path="/product/:id" component={TableContainer} />
        </Switch>
      </Router>

    </div>
  );
};


export default App;
