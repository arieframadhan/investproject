import React from 'react';
import convetionalInvoice from '../../DataJson/invoice/conventional_invoice';
import shariaInvoice from '../../DataJson/invoice/sharia_invoice';
import conventionalOsf from '../../DataJson/osf/conventional_osf';
import reksaDana from '../../DataJson/reksadana/reksadana';
import sbn from '../../DataJson/sbn/sbn';
import TableFeature from './TableFeaturure';

const TableContainer = (props) => {
  const { match: { params: id } } = props;
  return (
    <div>
      {
          id.id === 'SBN' ?
            <TableFeature payload={sbn} /> :
            id.id === 'Reksadana' ?
              <TableFeature payload={reksaDana} /> :
              id.id === 'OSF Financing' ?
                <TableFeature payload={conventionalOsf} /> :
                id.id === 'Conventional Invoice' ?
                  <TableFeature payload={convetionalInvoice} /> :
                  id.id === 'Sharia Invoice' ?
                    <TableFeature payload={shariaInvoice} /> :
                    null
        }
    </div>
  );
};

export default TableContainer;
