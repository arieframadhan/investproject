import React from 'react';
import { Collapse, Button, Row, Col, Alert } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Menu = (props) => {
  const { data, openCollapse, showCollapse, trigger } = props;
  const { name, count, type } = data;
  const styles = {
    container: {
      margin: '0 auto',
      marginTop: '10px',
      width: '900px',
      height: '100px',
      backgroundColor: 'white',
      border: '1px solid grey',
    },
  };
  return (
    <>
      <Link to={name !== 'Invoice Financing' ? `/product/${name}` : null}>
        <Button
          onClick={openCollapse}
          aria-controls="example-collapse-text"
          aria-expanded={showCollapse}
          style={styles.container}
        >
          <div className="row">
            <div className="col">

              <p style={{ color: 'green', textAlign: 'left', justifyContent: 'flex-start' }}>
                {name}
                {' '}
              </p>
            </div>
            <div className="col">

              <p style={{ color: 'green', textAlign: 'right', justifyContent: 'flex-start' }}>
                {count}
                {' '}
              </p>
            </div>
          </div>
          <p style={{ color: 'black', textAlign: 'left' }}>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
          </p>
        </Button>
      </Link>
      <Collapse in={showCollapse === true && name === trigger.data ? true : null}>
        <div id="example-collapse-text">
          {type !== undefined ?
            <Row style={{ marginLeft: 135, marginRight: 135 }}>
              {type.map((res) => (
                <>
                  <Col xs lg="4" style={{ border: '1px solid grey', width: '600px', height: '100px', margin: '0 auto', marginTop: 10 }}>

                    <Link to={`/product/${res.name}`}>
                      <Row>
                        <Col>
                          <p style={{ color: 'green', textAlign: 'left', justifyContent: 'flex-start' }}>
                            {res.name}
                          </p>

                        </Col>
                        <Col>
                          <p style={{ color: 'green', textAlign: 'right', justifyContent: 'flex-start' }}>
                            {res.count}
                          </p>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <p style={{ color: 'black', textAlign: 'left' }}>{res.name}</p>
                        </Col>
                      </Row>
                    </Link>
                  </Col>
                </>
              )) }


            </Row> :
            <Alert variant="danger">
          No Data!
            </Alert>}

        </div>
      </Collapse>
    </>
  );
};

export default Menu;
