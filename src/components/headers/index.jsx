import React from 'react';
import photo from '../../Assets/Banner.png';

const Header = () => {
  const styles = {
    container: {
      width: 900,
      height: 200,
      backgroundColor: 'red',
      margin: '0 auto',
      marginTop: '20px',
    },
    imgStyle: { width: '100%', height: '100%' },
  };
  return (
    <div style={styles.container}>
      <img src={photo} style={styles.imgStyle} />
    </div>
  );
};


export default Header;
