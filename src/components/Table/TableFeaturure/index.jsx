import React from 'react';
import { Table, Dropdown, Spinner, InputGroup, FormControl, Row, Col, Button } from 'react-bootstrap';


class TableFeature extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      valueDropDown: 'All',
      loading: false,
      dataGrade: ['A', 'A-', 'B+', 'B', 'B-', 'C+', 'C', 'C-', 'D'],
      search: '',
      message: '',
    };
  }

  componentDidMount() {
    this.fetch();
  }

  onChangeSearch = (e) => {
    const search = e.target.value;
    this.setState({ search });
  }

  searchMethod = () => {
    const { data, search } = this.state;
    const dataTable = Object.assign([], data);
    const payload = [];
    for (let i = 0; i < data.length; i++) {
      if (search.toLowerCase() === data[i].name.toLowerCase()) {
        payload.push(dataTable[i]);
        this.setState({ message: '' });
      } else {
        this.setState({ message: 'Data not found!' });
      }
    }
    if (payload.length > 0) {
      this.fetch(payload);
    } else {
      this.fetch();
    }
  }

  fetch = (data) => {
    const { payload: { contents } } = this.props;
    this.setState({ loading: true });
    if (data === undefined || data === '') {
      this.setState({ data: contents, loading: false });
    } else {
      this.setState({ data, loading: false, message: '' });
    }
  }

  onChangeGrade = (value) => {
    const { data } = this.state;
    const filterDataGrade = [];
    for (let i = 0; i < data.length; i++) {
      if (data[i].grade === value) {
        filterDataGrade.push(data[i]);
      }
    }
    this.fetch(filterDataGrade);
  }

  render() {
    const styles = {
      container: {
        width: 900,
        margin: '0 auto',
      },
      textStyle: {
        color: 'red',
        textAlign: 'left',
      },
    };
    const { valueDropDown, dataGrade, data, message, loading } = this.state;
    return (
      <div style={styles.container}>
        <Row>
          <Col>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text id="basic-addon1">@</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                onChange={this.onChangeSearch}
                placeholder="Search"
                aria-label="Username"
                aria-describedby="basic-addon1"
              />
              <InputGroup.Append>
                <Button onClick={this.searchMethod}> Search </Button>
              </InputGroup.Append>
            </InputGroup>
          </Col>
          <Col>
            <Dropdown>
              <Dropdown.Toggle variant="success" id="dropdown-basic">
                {valueDropDown}
              </Dropdown.Toggle>

              <Dropdown.Menu>
                {
              dataGrade.map((res) => (
                <Dropdown.Item key={Math.random()} onSelect={this.onChangeGrade} eventKey={res}>
                  {res}
                </Dropdown.Item>
              ))
            }
              </Dropdown.Menu>
            </Dropdown>
          </Col>
        </Row>

        <p style={styles.textStyle}>{message}</p>
        {
        loading === true ?
          <Spinner animation="grow" /> :
          null
      }
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Name</th>
              <th>Harga</th>
              <th>Grade & Rate</th>
              <th>Tenor</th>
            </tr>
          </thead>
          {
          data.length < 0 ?
            null :
            <tbody>
              {
              data.map((res) => (
                <tr key={Math.random()}>
                  <td>{res.name}</td>
                  <td>{res.amount}</td>
                  <td>
                    {res.grade}
                    {res.rate}
                  </td>
                  <td>{res.tenor}</td>
                </tr>
              ))
            }
            </tbody>
        }

        </Table>
      </div>
    );
  }
}

export default TableFeature;
